﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_CustomClass
{
    class BankAccount
    {
        /*
        Allen Boynton
        SDI1712 Section #03
        12/13/2017
        Custom Class Assignment
        */

        //Create member variables
        string mUserName;
        decimal mCheckingAcct;
        decimal mBankAcct;

        //Constructor for the member variables
        public BankAccount(string _userName, decimal _checkingAcct, decimal _bankAcct)
        {
            //Set the values of the member variables
            this.mUserName = _userName;
            this.mCheckingAcct = _checkingAcct;
            this.mBankAcct = _bankAcct;
        }

        //Getters
        public string GetUserName()
        {
            return mUserName;
        }

        public decimal GetCheckingAcct()
        {
            return mCheckingAcct;
        }

        public decimal GetBankAcct()
        {
            return mBankAcct;
        }

        //Setters
        public void SetUserName(string _userName)
        {
            this.mUserName = _userName;
        }

        public void SetCheckingAcct(decimal _checkingAcct)
        {
            this.mCheckingAcct = _checkingAcct;
        }

        public void SetBankAcct(decimal _bankAcct)
        {
            this.mBankAcct = _bankAcct;
        }

        //Custom method will add the checking and banking accounts together
        //Parameters are checkingAcctValue and bankingAcctValue
        //Returns a grand total - decimal
        public string CalculateGrandTotal(decimal checkBalance, decimal bankBalance)
        {
            decimal totalBalance = checkBalance + bankBalance;

            string grandTotalFormatted = totalBalance.ToString("C");

            return grandTotalFormatted;
        }
    }
}
