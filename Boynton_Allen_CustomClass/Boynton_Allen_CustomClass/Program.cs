﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             Allen Boynton
             SDI1712 Section #03
             12/13/2017
             Custom Class Assignment
             */

            //Problem #1: Bank Account
            
            Console.WriteLine("\r\nWelcome to your Bank Account!\r\n\r\nHere we will collect your " +
                "bank balance information,\r\nconduct 3 transactions with running totals for each " +
                "account\r\nand give you a grand total of both of your accounts combined.\r\nLet's go!\r\n");

            //Prompt the user for their full name
            Console.WriteLine("Please enter your full name.");
            //Store the user's input in a string variable
            string userNameStr = Console.ReadLine();

            //Conditional will verify entry is not blank
            while (string.IsNullOrWhiteSpace(userNameStr))
            {
                //Communicate user that they must enter their name
                Console.WriteLine("You left your entry blank.\r\nPlease enter your full name.");
                //Store inut in a string variable
                userNameStr = Console.ReadLine();
            }

            //Prompt user for their starting checking account balance
            Console.WriteLine($"Hello {userNameStr}! Please enter your current Checking account balance.");
            //Store the user's input in a string variable
            string checkingAcctStr = Console.ReadLine();

            //Create a decimal variable to store the converted string entry
            decimal checkingAcctValue;

            //Conditional to check if a number was entered
            while (!decimal.TryParse(checkingAcctStr, out checkingAcctValue))
            {
                //Inform user they did not enter a valid number
                Console.WriteLine("You did not enter a number.\r\nPlease enter your current Checking balance.");
                //Store input
                checkingAcctStr = Console.ReadLine();
            }

            //Prompt user for their starting savings account balance
            Console.WriteLine("Now, enter your current Saving's account balance.");
            //Store the user's input in a string variable
            string bankAcctStr = Console.ReadLine();

            //Create a decimal variable to store the converted string entry
            decimal bankAcctValue;

            //Conditional to check if a number was entered
            while (!decimal.TryParse(bankAcctStr, out bankAcctValue))
            {
                //Inform user they did not enter a valid number
                Console.WriteLine("You did not enter a number.\r\nPlease enter your current Saving's balance.");
                //Store input
                bankAcctStr = Console.ReadLine();
            }

            //Instantiate an instance of the BankAccount object
            BankAccount bankAccount = new BankAccount(userNameStr, checkingAcctValue, bankAcctValue);

            //Alert the user of the starting balances they input
            Console.WriteLine($"{bankAccount.GetUserName()}, you entered your Checking balance as " +
                $"{bankAccount.GetCheckingAcct().ToString("C")}, and your Saving's balance as " +
                $"{bankAccount.GetBankAcct().ToString("C")}.\r\nThis gives you a grand total of both " +
                $"of your accounts combined as " +
                $"{bankAccount.CalculateGrandTotal(bankAccount.GetCheckingAcct(), bankAccount.GetBankAcct())}.");

            //Crewate a loop that will repeat 3 times
            for (int i = 0; i < 3; i++)
            {
                //Calculate transaction number
                int transactionCount = i + 1;

                //Available balance
                decimal availableInChecking = bankAccount.GetCheckingAcct();
                decimal availableInSavings = bankAccount.GetBankAcct();

                //Prompt user if they want to deposit or withdraw
                Console.WriteLine($"\r\nPlease enter (1) for Withdrawal or (2) for Deposit for transaction #{transactionCount}.");
                string transactionType = Console.ReadLine();

                //Variable is used for converted value
                int transTypeValue;

                //Conditional checks if a valid choice was picked
                while ((!int.TryParse(transactionType, out transTypeValue)) && (!transactionType.Equals("1") ||
                    (transactionType.Equals("2"))))
                {
                    //Inform the user their entry is invalid
                    Console.WriteLine("Not a valid entry.\r\nPlease enter a (1) for Withdrawal or (2) for Deposit.");
                    //Store the entry
                    transactionType = Console.ReadLine();
                }

                //Prompt user for which account they wish to make the change to
                Console.WriteLine($"For which account would you like to use for transaction #{transactionCount}?\r\n" +
                    "Enter (1) for Checking or (2) for Savings.");
                string accountChoice = Console.ReadLine();

                //Variable used for converted value of entry
                int acctChoiceValue;

                //Conditional checks if a valid choice was picked
                while ((!int.TryParse(accountChoice, out acctChoiceValue)) && (!accountChoice.Equals("1") ||
                    (accountChoice.Equals("2"))))
                {
                    //Inform the user their entry is invalid
                    Console.WriteLine("Not a valid entry.\r\nPlease enter a (1) for Checking or (2) for Savings.");
                    //Store the entry
                    accountChoice = Console.ReadLine();
                }

                //If statements checks the user's entry to determine type of transaction and what account
                //If statement determines withdrawal or deposit
                if (transTypeValue == 1)
                {
                    //User chooses to make a withdrawal
                    //Prompt user for the amount of their withdrawal
                    Console.WriteLine("How much is your withdrawal?");
                    string withdrawAmtString = Console.ReadLine();

                    //Decimal used to store the converted amount
                    decimal withdrawAmount;

                    //Verify the user's input is a number
                    while (!decimal.TryParse(withdrawAmtString, out withdrawAmount))
                    {
                        //Display to the user they did not enter a valid number
                        Console.WriteLine("Not a valid entry.\r\nPlease enter an amount to withdraw.");
                        withdrawAmtString = Console.ReadLine();
                    }

                    //If statement determines which account the user wants to access
                    if (acctChoiceValue == 1)
                    {
                        //Conditional check user request with balance available
                        if ((availableInChecking - withdrawAmount) >= 0)
                        {
                            //User chooses their checking account
                            availableInChecking -= withdrawAmount;

                            //Set new checking balance
                            bankAccount.SetCheckingAcct(availableInChecking);

                            //Display checking balance to the user
                            Console.WriteLine($"For transaction #{transactionCount}, your new Checking balance is " +
                                $"{bankAccount.GetCheckingAcct().ToString("C")}.\r\n" +
                                $"Your Saving's balance is still {bankAccount.GetBankAcct().ToString("C")}.");
                        }
                        else
                        {
                            //Output to the user that they do not have sufficient funds to withdraw this amount
                            Console.WriteLine($"For transaction #{transactionCount}, your Checking balance of {availableInChecking.ToString("C")} " +
                                $"does not allow you to withdraw {withdrawAmount.ToString("C")}.\r\n" +
                                $"Your Saving's balance is still {bankAccount.GetBankAcct().ToString("C")}.");
                        }
                    }
                    else
                    {
                        //Conditional check user request with balance available
                        if ((availableInSavings - withdrawAmount) >= 0)
                        {
                            //User chooses their savings account
                            availableInSavings -= withdrawAmount;

                            //Set new savings balance
                            bankAccount.SetBankAcct(availableInSavings);

                            //Display savings balance to the user
                            Console.WriteLine($"For transaction #{transactionCount}, your new Saving's balance is " +
                                $"{bankAccount.GetBankAcct().ToString("C")}.\r\n" +
                                $"Your Checking balance is still {bankAccount.GetCheckingAcct().ToString("C")}.");
                        }
                        else
                        {
                            //Output to the user that they do not have sufficient funds to withdraw this amount
                            Console.WriteLine($"For transaction #{transactionCount}, your Saving's balance of {availableInSavings.ToString("C")} " +
                                $"does not allow you to withdraw {withdrawAmount.ToString("C")}.\r\n" +
                                $"Your Checking balance is still {bankAccount.GetCheckingAcct().ToString("C")}.");
                        }
                    }
                }
                else
                {
                    //User chooses to make a deposit
                    //Prompt user for the amount of their deposit
                    Console.WriteLine("How much is your deposit?");
                    string depositAmtString = Console.ReadLine();

                    //Decimal used to store the converted amount
                    decimal depositAmount;

                    //Verify the user's input is a number
                    while (!decimal.TryParse(depositAmtString, out depositAmount))
                    {
                        //Display to the user they did not enter a valid number
                        Console.WriteLine("Not a valid entry.\r\nPlease enter an amount to deposit.");
                        depositAmtString = Console.ReadLine();
                    }

                    //If statement determines which account the user wants to access
                    if (acctChoiceValue == 1)
                    {
                        //User chooses to deposit in their Checking
                        availableInChecking += depositAmount;

                        //Add amount to the checking balance
                        bankAccount.SetCheckingAcct(availableInChecking);

                        //Display checking balance to the user
                        Console.WriteLine($"For transaction #{transactionCount}, your new Checking balance is " +
                            $"{bankAccount.GetCheckingAcct().ToString("C")}.\r\n" +
                            $"Your Saving's balance is still {bankAccount.GetBankAcct().ToString("C")}.");
                    }
                    else
                    {
                        //User chooses to deposit in their Savings
                        availableInSavings += depositAmount;

                        //Add amount to the savings balance
                        bankAccount.SetBankAcct(availableInSavings);

                        //Display savings balance to the user
                        Console.WriteLine($"For transaction #{transactionCount}, your new Saving's balance is " +
                            $"{bankAccount.GetBankAcct().ToString("C")}.\r\n" +
                            $"Your Checking balance is still {bankAccount.GetCheckingAcct().ToString("C")}.");
                    }
                }
            }

            //Final output to give the user their total balance
            Console.WriteLine($"\r\n{bankAccount.GetUserName()}, your 3 transactions are complete.\r\n" +
                $"This gives you a grand total of both of your accounts combined as " +
            $"{bankAccount.CalculateGrandTotal(bankAccount.GetCheckingAcct(), bankAccount.GetBankAcct())}.\r\n");
        }
    }
}
