﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Allen Boynton
             * SDI1712 Section #03
             * 12/4/2017
             * Total Project
             */

            //Local variables
            string rectLengthString;
            string rectWidthString;
            string rectHeightString;

            double area;
            double perimeter;
            double volume;

            string[] messages = new string[] {"Enter a valid length in inches.",
            "Enter a valid width in inches.", "Enter a valid height in inches."};

            //Prompt, validate, and output to the user for length
            Console.WriteLine("\r\nEnter the length of the rectangle in inches:");
            rectLengthString = Console.ReadLine();
            bool canConvertL = double.TryParse(rectLengthString, out double rectLengthValue);
            ValidateInput(canConvertL, rectLengthString, rectLengthValue, messages[0]);
            Console.WriteLine("\r\nLength entered is: {0}\"\r\n", rectLengthValue);

            //Prompt, validate, and output to the user for width
            Console.WriteLine("Enter the width of the rectangle in inches:");
            rectWidthString = Console.ReadLine();
            bool canConvertW = double.TryParse(rectWidthString, out double rectWidthValue);
            ValidateInput(canConvertW, rectWidthString, rectWidthValue, messages[1]);
            Console.WriteLine("\r\nWidth entered is: {0}\"\r\n", rectWidthValue);

            //Calculate the area
            area = CalculateArea(rectLengthValue, rectWidthValue);
            Console.WriteLine("Area of the Rectangle is: {0} square inches.\r\n", area);

            //Calculate the perimeter
            perimeter = CalculatePerimeter(rectLengthValue, rectWidthValue);
            Console.WriteLine("Perimeter of the Rectangle is: {0}\"\r\n", perimeter);

            //Prompt, validate, and output to the user for height
            Console.WriteLine("Let's find the volume of this rectangle if it had a height.\r\nEnter a height in inches:");
            rectHeightString = Console.ReadLine();
            bool canConvertH = double.TryParse(rectHeightString, out double rectHeightValue);
            ValidateInput(canConvertH, rectHeightString, rectHeightValue, messages[1]);
            Console.WriteLine("\r\nTotal dimensions of the Rectangular Prism are: {0}\"L x {1}\"W x {2}\"H\r\n", rectLengthValue, rectWidthValue, rectHeightValue);

            //Calculate the volume
            volume = CalculateVolume(rectLengthValue, rectWidthValue, rectHeightValue);
            Console.WriteLine("Volume of the Rectangular Prism is: {0} cubic inches.\r\n", volume);

        }

        public static double CalculateArea(double length,double width)
        {
            double area = Math.Round(length * width, 2);

            return area;
        }

        public static double CalculatePerimeter(double length,double width)
        {
            double perimeter = Math.Round((length * 2) + (width * 2), 2);

            return perimeter;
        }

        public static double CalculateVolume(double length, double width,double height)
        {
            double volume = Math.Round(length * width * height, 2);

            return volume;
        }

        public static double ValidateInput(bool convert, string prompt, double value, string message)
        {
            while (!convert)
            {
                Console.WriteLine(message);
                prompt = Console.ReadLine();
            }

            return value;
        }
    }
}
