﻿// Name: Allen Boynton
// Date: 1712
// Course: Project & Portfolio 1
// Synopsis: This project is made up of my coding challenges.
// Finalized 12/14/2017

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Program
    {
        public static void Main(string[] args)
        {
            //Welcome user
            Console.WriteLine("\r\nWelcome to Coding Challenge!");
            
            {
                //Create an array of the challenges in this program
                string[] projectName = new string[] { "SwapInfo", "Backwards", "AgeConvert", "TempConvert" };
                //Declare variable to read user's choice of program to use
                string userPick = "";
                //Variable to store the converted value of the user's input
                int userPickedNumber = 0;

                //Instruction to the user
                Console.WriteLine("\r\nPlease choose which Coding Challenge you would like to enter.\r\n");

                //For loop will display the options for the menu
                for (int i = 0; i < projectName.Length; i++)
                {
                    //Looped message will display all menu options
                    Console.WriteLine($"Enter ({i + 1}) for {projectName[i]}.");
                }
                //Store user's input as a string
                userPick = Console.ReadLine();

                //Increment to verify the user's input for menu title selected
                int increment = 1;
                while ((!int.TryParse(userPick, out userPickedNumber)) && (userPickedNumber != increment))
                {
                    //Display to the user that they did not enter a valid number choice
                    Console.WriteLine("Please enter a valid number choice.");
                    userPick = Console.ReadLine();

                    //Increments through the user input choice
                    increment++;
                }

                //Conditonal to match the user's response to the menu title selection
                if (userPickedNumber == 1)
                {
                    //Call SwapInfo challenge
                    SwapInfo.SwapFirstAndLastName();
                }
                else if (userPickedNumber == 2)
                {
                    //Call Backwards challenge
                    Backwards.ReverseUserSentence();
                }
                else if (userPickedNumber == 3)
                {
                    //Call AgeConvert challenge
                    AgeConvert.ConvertAgeToTimeIntervals();
                }
                else if (userPickedNumber == 4)
                {
                    ////Call TempConvert challenge
                    TempConvert.HandleChangesInTemperature();
                }
            }
        }

        //Custom method used to ask the user if they wish to continue once a program has run
        public static void RestartMainMethod()
        {
            //Display to the user if they want to start over
            Console.WriteLine("Would you like to start from the beginning?\r\n" +
            "Enter (Y) for YES or (N) for NO.");
            string replay = Console.ReadLine();

            //Local variable to read the user's input to start over or not - ignores case
            bool answerY = string.Equals(replay, "Y", StringComparison.InvariantCultureIgnoreCase);
            bool answerN = string.Equals(replay, "N", StringComparison.InvariantCultureIgnoreCase);

            //Conditional validates user's input of Y or N
            while ((string.IsNullOrWhiteSpace(replay)) && (!answerY) && (!answerN))
            {
                //Instructs the user that they entered an invalid choice and to choose again
                Console.WriteLine("Please enter (Y) to restart or (N) to quit.");
                replay = Console.ReadLine();
            }

            //Conditional calls the Main method if Y is picked
            if (answerY)
            {
                //Calls the Main method
                Main(null);
            }
        }
    }
}
