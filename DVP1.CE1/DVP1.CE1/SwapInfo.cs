﻿// Name: Allen Boynton
// Date: 12/4/2017
// Course: Project & Portfolio 1
// Synopsis: Prompts the user for their first and last name. 
//           Display it then write a method that reverses it.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class SwapInfo
    {
        public static void SwapFirstAndLastName()
        {
            //Identifies the program to be run
            Console.WriteLine("\r\nWelcome to the SwapInfo Challenge!\r\n");
            
            //Prompt user and store input in a new variable
            Console.WriteLine("Enter your first name");
            string firstName = Console.ReadLine();

            //Validate the entry
            while (string.IsNullOrWhiteSpace(firstName))
            {
                //Instruct the user they made an invalid choice and to enter their first name
                Console.WriteLine("Please do not leave this blank.\r\nEnter your first name");
                firstName = Console.ReadLine();
            }

            //Prompt user and store last name in a new variable
            Console.WriteLine("Enter your last name");
            string lastName = Console.ReadLine();

            //Conditional to check entry is not empty
            while (string.IsNullOrWhiteSpace(lastName))
            {
                Console.WriteLine("Please do not leave this blank.\r\nEnter your last name");
                lastName = Console.ReadLine();
            }

            //Print outputs
            Console.WriteLine($"Your first name is {firstName}.");
            Console.WriteLine($"Your last name is {lastName}.");
            Console.WriteLine($"Your full name is {firstName} {lastName}.");

            //Call method to swap first and last name
            HandleSwapInfo(lastName, firstName);

            //Call a custom method to return to the Main method if the user wants to continue
            Program.RestartMainMethod();
        }

        //Method which reverses the order
        public static void HandleSwapInfo(string lastName, string firstName)
        {
            //Output to the console to swap names
            Console.WriteLine($"Your full name reversed is {lastName} {firstName}.\r\n");
        }
    }
}