﻿// Name: Allen Boynton
// Date: 12/10/2017
// Course: Project & Portfolio 1
// Synopsis: TempConvert - Convert F to C and C to F

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class TempConvert
    {
        public static void HandleChangesInTemperature()
        {
            //Display the challenge that the user has chosen
            Console.WriteLine("\r\nWelcome to the TempConvert\u00b0 Challenge!\r\n");

            //Prompt the user to enter a temperature in Celcius
            Console.WriteLine("Please enter the temperature in Celcius\u00b0 you wish to convert.");
            string celciusString = Console.ReadLine();

            //Declare double variable to store a converted input from string
            //Using double in case the user adds decimal places
            double celciusValue;

            //Conditional to verify the user entered a valid number
            while (!double.TryParse(celciusString, out celciusValue))
            {
                //Instructs the user they made an invalid entry and to enter again
                Console.WriteLine("A valid temperature value was not entered.\r\n" +
                    "Please enter a valid temperature in degrees Celcius.");
                celciusString = Console.ReadLine();
            }

            //Prompt the user to enter a temperature in Fahrenheit
            Console.WriteLine("Please enter the temperature in Fahrenheit\u00b0 you wish to convert.");
            string fahrenheitString = Console.ReadLine();

            //Declare double variable to store a converted input from string
            //Using double in case the user adds decimal places
            double fahrenheitValue;

            //Conditional to verify the user entered a valid number
            while (!double.TryParse(fahrenheitString, out fahrenheitValue))
            {
                //Instructs the user they made an invalid entry and to enter again
                Console.WriteLine("A valid temperature value was not entered.\r\n" +
                    "Please enter a valid temperature in degrees Fahrenheit.");
                fahrenheitString = Console.ReadLine();
            }

            //Call the custom method to insert argument and return a converted value as F
            Console.WriteLine($"You entered a Celcius value of {celciusString}\u00b0C.\r\n" +
                $"The converted temperature in Fahrenheit is " +
                $"{CalculateToFahrenheit(celciusValue)}\u00b0F.\r\n");

            //Call the custom method to insert argument and return a converted value as C
            Console.WriteLine($"You entered a Fahrenheit value of {fahrenheitString}\u00b0F.\r\n" +
                $"The converted temperature in Celcius is " +
                $"{CalculateToCelcius(fahrenheitValue)}\u00b0C.\r\n");

            //Call a custom method to ask the user if they wish to start over or quit
            Program.RestartMainMethod();
        }

        //Custom methods to calculate the temperature conversion
        //Takes in a parameters of celcius temperature
        //Returns temperature as double
        public static double CalculateToFahrenheit(double celcius)
        {
            return Math.Round((1.8 * celcius) + 32.0, 2);
        }

        //Takes in a parameters of fahrenheit temperature
        //Returns temperature as double
        public static double CalculateToCelcius(double fahrenheit)
        {
            return Math.Round((((fahrenheit - 32.0) * 5.0) / 9.0), 2);
        }
    }
}
