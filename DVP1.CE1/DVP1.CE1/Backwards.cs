﻿// Name: Allen Boynton
// Date: 12/4/2017
// Course: Project & Portfolio 1
// Synopsis: Prompt the user for at least a 6 word sentence. Then reverse it.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Backwards
    {
        public static void ReverseUserSentence()
        {
            //Display the program chosen to the user
            Console.WriteLine("\r\nWelcome to the Backwards Challenge!\r\n");

            //Prompt user to enter a sentence and stored in a new variable
            Console.WriteLine("Enter in a sentence with at least 6 words: ");
            string userSentence = Console.ReadLine();

            //Check for at least 5 spaces which would designate at least a 6 word sentence
            char[] sentenceArray = userSentence.ToCharArray();

            //Conditional check to verify sentence entered is at least 6 words
            //Checks that there are at least 5 white spaces - which would verify at least 6 words
            while (!(CountSpaces(userSentence) >= 5))
            {
                //Instruct usewr of an invalid entry. Please enter again
                Console.WriteLine("Invalid entry.\r\nEnter a sentence of at least 6 words:");
                userSentence = Console.ReadLine();
            }

            //Display the sentence the user entered
            Console.WriteLine($"User's sentence: {userSentence}");

            //Use Reverse() to reverse the sentence
            Console.WriteLine($"Reversed sentence: {ConvertReverseString(userSentence)}\r\n");

            //Call a custom method to check if the user would like to continue from the beginning
            Program.RestartMainMethod();
        }

        //Method iterates through the string and finds number of spaces
        public static int CountSpaces(string userString)
        {
            //Variables for character number and a space char
            int charCount = 0;
            string spaceString;

            //Loop through each character
            for (int i = 0; i < userString.Length; i++)
            {
                //Reduce sentence by 1 character as it loops
                spaceString = userString.Substring(i, 1);

                //If statement counts number of spaces
                if (spaceString == " ")
                {
                    //Add the spaces
                    charCount++;
                }
            }
            //Returns number of spaces as an integer
            return charCount;
        }

        //Create a char array from the user sentence and reverse the order using Reverse() an Array property
        public static string ConvertReverseString(string userSentence)
        {
            //Create an array of char from string
            char[] sentenceArray = userSentence.ToCharArray();
            //Reverse the order of the char array
            Array.Reverse(sentenceArray);
            //Store reversed char array into a new variable
            string reversedSentence = new string(sentenceArray);

            //Returns a converted reversed char array as a string
            return reversedSentence;
        }
    }
}