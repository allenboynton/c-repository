﻿// Name: Allen Boynton
// Date: 12/5/2017
// Course: Project & Portfolio 1
// Synopsis: AgeConvert project. Prompts the user for their name 
// and age. Then, age is converted age to number of days

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class AgeConvert
    {
        public static void ConvertAgeToTimeIntervals()
        {
            //Displays the program chosen
            Console.WriteLine("\r\nWelcome to the AgeConvert Challenge!\r\n");

            //Prompt the user for their name and age in number of years
            Console.WriteLine("Please enter your name: ");
            string userName = Console.ReadLine();

            //Conditional to check that the user entered a name
            while (string.IsNullOrWhiteSpace(userName))
            {
                //Instructs the user they made an invalid entry and prompts again
                Console.WriteLine("No entry provided.\r\nPlease enter your name: ");
                userName = Console.ReadLine();
            }

            Console.WriteLine("Please enter your age in years: ");
            string userAgeString = Console.ReadLine();
            
            //Variable to store the age value
            double userAgeValue;

            //Conditional to verify a number is entered
            while ((!double.TryParse(userAgeString, out userAgeValue)) || (string.IsNullOrWhiteSpace(userAgeString)))
            {
                //Response to the user
                Console.WriteLine("You did not enter a number.\r\nPlease enter your age in number of years: ");
                userAgeString = Console.ReadLine();
            }

            //Displaying the name and age entered
            Console.WriteLine($"\r\nHello {userName}! You entered your age as {userAgeString} years old.");

            //Display string variables
            string ageDisplayString = $"{userName} has been alive for ";
            //Array of the time intervals
            string[] timeIntervals = new string[] { " days.", " hours.", " minutes.", " seconds." };

            //Store calculaton in a local variable and calculations for given times
            double ageInDays = userAgeValue * 365.0;
            double ageInHours = ageInDays * 24;
            double ageInMinutes = ageInHours * 60;
            double ageInSeconds = ageInMinutes * 60;

            //Variables added to a double array to loop through to display to the user
            double[] ageConvertedArray = new double[] { ageInDays, ageInHours, ageInMinutes, ageInSeconds };

            //Use a for loop to cycle through arrays to create displays
            for (int i = 0; i < ageConvertedArray.Length; i++)
            {
                //Output to the console 
                Console.WriteLine(ageDisplayString + ageConvertedArray[i] + timeIntervals[i]);
            }

            //Calls a custom method to check if the user wishes to restart from the Main method
            Program.RestartMainMethod();
        }
    }
}
