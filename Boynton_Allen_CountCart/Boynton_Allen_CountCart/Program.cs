﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_CountCart
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Allen Boynton
            SDI1712 Section #03
            12/4/2017
            Count The Cart Assignment
            */

            //Declare and define an array of strings
            string[] shoppingCart = new string[] {"snack","drink","vegetable","drink",
                "meat","snack","vegetable","snack","drink","drink"};
            
            //Create shopping cart menu for the user
            Console.WriteLine("\r\nPlease choose one of the following, then press Return.\r\n" +
                "(1) for Snacks\r\n(2) for Drinks\r\n(3) for Vegetables\r\n(4) for Meats");
            //Store input in variable - as a string
            string menuChoice = Console.ReadLine();
            
            //Number variable to store the user's input
            int menuValue;

            //Conditional verifies input is an integer and not empty
            //Need to verify that input entered is 1-4
            while (!(int.TryParse(menuChoice, out menuValue)) ||
                string.IsNullOrWhiteSpace(menuChoice) || 
                menuValue < 1 || menuValue > 4)
            {
                //Prompt user and store input into a variable
                Console.WriteLine("Please enter a valid number 1-4.");
                menuChoice = Console.ReadLine();
            }

            //Perform stacked loops and search for each element in each loop
            //Set up for the iteration
            for (int i = 0; i < shoppingCart.Length; i++)
            {
                //Verifies the number the user entered
                if (menuValue == i + 1)
                {
                    //Start at 1 so the array length is the same as the array count
                    int count = 1;
                    
                    //Second loop will iterate 2 elements that are not the same
                    for (int j = 0; j < shoppingCart.Length; j++)
                    {
                        //Conditional checks that 2 values are not the same
                        if (i != j && (shoppingCart[i] == shoppingCart[j]))
                        {
                            //If they are not the same then add 1 to the count
                            count++;
                        }
                    }

                    //Output to the user
                    Console.WriteLine($"In the shopping cart there are {count}" +
                        $" {shoppingCart[i]}s.\r\nWhere {count} is the number of items.\r\n" +
                        $"Where {shoppingCart[i]} is the name of the item.");
                }
            }
        }
    }
}
