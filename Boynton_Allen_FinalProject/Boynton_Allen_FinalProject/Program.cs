﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI1712 Section #03
             12/12/2017
             Final Project
             */

            //Welcome the user and exsplain the purpose of the program
            Console.WriteLine("Welcome to your gas calculator!\r\nWe will be " +
                "calculating the cost of gas spent on multiple car trips.\r\n");

            //Prompt the user for number of trips they want to take
            Console.WriteLine("How many car trips do you wish to take?");

            //Strore the user's response in a variable
            string numOfCarTripsString = Console.ReadLine();

            //Create an integer to convert the user's response to
            int numOfCarTrips;

            //Conditional to validate the user's input is a number
            while (!int.TryParse(numOfCarTripsString, out numOfCarTrips))
            {
                //Communicate wit the user and inform them of the error in their input
                Console.WriteLine("You did not enter a number.\r\nPlease " +
                    "enter a number of car trips you wish to take.");
                //Store user's re-entry
                numOfCarTripsString = Console.ReadLine();
            }
            
            //Call TotalMiles method and send variable as an argument
            double totalMiles = TotalMiles(GetMiles(numOfCarTrips));

            //Prompt user for their MPG
            Console.WriteLine("Please enter your vehicle's Miles Per Gallon (MPG).");
            string milesPerGalString = Console.ReadLine();

            //Integer is converted from the user's input
            int milesPerGallon;

            //Conditional to validate the user's input is a number
            while (!int.TryParse(milesPerGalString, out milesPerGallon))
            {
                //Communicate wit the user and inform them of the error in their input
                Console.WriteLine("You did not enter a number.\r\nPlease " +
                    "Enter a number for your vehicle's Miles Per Gallon (MPG).");
                //Store user's re-entry
                milesPerGalString = Console.ReadLine();
            }

            //Prompt user for the cost of gas per gallon
            Console.WriteLine("Please enter your cost of gas per gallon.");
            string pricePerGalString = Console.ReadLine();

            //Decimal is converted from the user's input
            decimal pricePerGallon;

            //Conditional to validate the user's input is a number
            while (!decimal.TryParse(pricePerGalString, out pricePerGallon))
            {
                //Communicate wit the user and inform them of the error in their input
                Console.WriteLine("You did not enter a number.\r\nPlease " +
                    "Enter a number for your cost of gas per gallon.");
                //Store user's re-entry
                pricePerGalString = Console.ReadLine();
            }

            //Store the CostOfGas in a string for formatting
            string costOfGasFormatted = CostOfGas(totalMiles, milesPerGallon, pricePerGallon).ToString("C");

            //Final output to the user
            Console.WriteLine($"The total cost of driving {totalMiles} miles is " +
                $"{costOfGasFormatted}.");

            /*
            Test, Test, Test
            Example given #1:
            i.   # of trips = 3
            ii.  1st Trip = 200, 2nd Trip = 100, 3rd Trip = 30
            iii. MPG = 30
            iv.  Cost of 1 gallon of gas is $2.20
            v.   Final Output should be:
                1. “The total cost of driving 330 miles is $24.20.”

            Example given #2:
            i.   # of trips = 2
            ii.  1st Trip = 98.66, 2nd Trip = 123.67
            iii. MPG = 15
            iv.  Cost of 1 gallon of gas is $2.59
            v.   Final Output should be:
                1. “The total cost of driving 222.33 miles is $38.39.”

            Example given #3:
            i.   # of trips = 4
            ii.  1st Trip = 34.5, 2nd Trip = 65.25, 3rd Trip = 79.8, 4th Trip = 122.9
            iii. MPG = 24
            iv.  Cost of 1 gallon of gas is $2.39
            v.   Final Output should be:
                1. “The total cost of driving 302.45 miles is $30.12.”

            Example given #4:
            i.   # of trips = 3
            ii.  1st Trip = 284, 2nd Trip = 142.6, 3rd Trip = 379.1
            iii. MPG = 36
            iv.  Cost of 1 gallon of gas is $1.99
            v.   Final Output should be:
                1. “The total cost of driving 805.7 miles is $44.54.”
            */

        }

        //Custom method to take in the number of trips 
        //Handles prompting user for the miles for each trip
        //Return an array of the miles for each trip
        public static double[] GetMiles(int trips)
        {
            //Int array will be the length of the number of trips
            double[] milesArray = new double[trips];

            //Loop will ask the user for miles per trip for as many times as the number of trips
            for (int i = 0; i < trips; i++)
            {
                //Prompt the user for mileage per trip
                Console.WriteLine($"Please enter your mileage for trip {i + 1}.");
                //Store the user's input in the array
                string milesPerTripString = Console.ReadLine();

                //While conditional to verify number of miles
                while (!double.TryParse(milesPerTripString, out milesArray[i]))
                {
                    //Display a response to the user
                    Console.WriteLine($"{milesPerTripString} is not a number.\r\n" +
                        $"Please enter the number of miles for trip {i + 1}.");
                    //Store user's input
                    milesPerTripString = Console.ReadLine();

                    Console.WriteLine($"Number of miles for trip number {i + 1} is {milesPerTripString}");
                }
            }
            //Return the array of miles
            return milesArray;
        }

        //Custom method takes in an array of doubles (miles)
        //Adds up all the elements of the array
        //Returns a double of the total miles
        public static double TotalMiles(double[] miles)
        {
            //Define a double to store the total miles
            double sum = 0;

            //Loop will add the elements together
            for (int i = 0; i < miles.Length; i++)
            {
                //Iterates through the array and adds up the elements
                sum += miles[i];
            }
            //Return the total miles to the Main method
            return sum;
        }

        //Custom method takes in total miles, MPG and gas cost
        //Calculates the total cost of the total trips
        //Returns total cost as a decimal
        public static decimal CostOfGas(double miles, int mpg, decimal cost)
        {
            //Store the calculation in a decimal variable
            decimal totalCost = (decimal)(miles / mpg) * cost;
            
            //Return cost as a decimal to the Main method
            return totalCost;
        }
    }
}
