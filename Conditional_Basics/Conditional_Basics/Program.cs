﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditional_Basics
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Allen Boynton
            SDI 1712 Section #03
            11/30/2017
            Steak Temperature Example
            */

            /*
            Steak Temperatures
            Rare        130-140 F
            Medium Rare 140-150 F
            Medium      150-160 F
            Well Done   160-170 F
            */

            //Start of constant response
            string returnResponse = "Your steak will be cooked ";

            //Ask user how they would like their steak cooked
            Console.WriteLine("\r\nHow would you like your steak cooked?\r\nAt what temperature do you want?");

            //Store the user's response in a string var
            string steakTempString = Console.ReadLine();

            //Convert string to a number data type
            int steakTemp = int.Parse(steakTempString);

            //String to eliminate a repetitive response
            string steakReadyResponse = returnResponse + steakTempString;

            //Else if statement will filter through the temps to find the correct doneness
            if (steakTemp < 130)
            {
                Console.WriteLine("This meat is just raw!");
            }
            else if (steakTemp < 140)
            {
                steakTempString = "Rare.";
                Console.WriteLine(steakReadyResponse);
            }
            else if (steakTemp < 150)
            {
                steakTempString = "Medium Rare.";
                Console.WriteLine(steakReadyResponse);
            }
            else if (steakTemp < 160)
            {
                steakTempString = "Medium.";
                Console.WriteLine(steakReadyResponse);
            }
            else if (steakTemp <= 170)
            {
                steakTempString = "Well Done.";
                Console.WriteLine(steakReadyResponse);
            }
            else
            {
                Console.WriteLine("This meat is BURNT!");
            }














        }
    }
}
