﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI1712 Section #03
             12/2/2017
             Logic & Loops Assignment
            */

            //Problem #1-Logical Operators: Making The Grade

            Console.WriteLine("\r\nWelcome to Making the Grade!\r\n");

            //Variables to store the string input
            string firstGradeString;
            string secondGradeString;

            //Array with 2 elements stored - as ints
            int[] gradeValueArray = new int[2];

            //Prompt user for their first grade
            Console.WriteLine("Please enter your first grade.");
            firstGradeString = Console.ReadLine();

            //Verified numbers can be converted and it is not blank
            while ((string.IsNullOrWhiteSpace(firstGradeString)) ||
                    (!(int.TryParse(firstGradeString, out gradeValueArray[0]))))
            {
                Console.WriteLine("Please enter a valid number grade.");
                firstGradeString = Console.ReadLine();
            }

            //Prompt user for their second grade
            Console.WriteLine("Please enter your second grade.");
            secondGradeString = Console.ReadLine();

            //Verified numbers can be converted and it is not blank
            while ((string.IsNullOrWhiteSpace(secondGradeString)) ||
                    (!(int.TryParse(secondGradeString, out gradeValueArray[1]))))
            {
                //Inform the user and ask to enter again
                Console.WriteLine("Please enter a valid number grade.");
                secondGradeString = Console.ReadLine();
            }

            /*
            Conditional if/else checking first grade AND second grade determines 
            if both grades are 70 or above or lower than 70
            */
            if ((gradeValueArray[0] >= 70) && (gradeValueArray[1] >= 70))
            {
                Console.WriteLine("Congratulations, both grades are passing!");
            }
            else
            {
                Console.WriteLine("One or more grades are failing, try harder next time!");
            }

            /*
            Data Sets To Test:
            Grade 1 – 95 Grade 2 – 85 Results – "Congrats, both grades are passing!"
            Grade 1 – 82 Grade 2 – 68 Results – "One or more grades are failing, try
            harder next time!"
            Grade 1 – Eighty Computer should Re-prompt , Grade1 – 80 Grade 2 - 91
            Results – "Congrats, both grades are passing!"

            Self Test:
            Grade 1 - 70 Grade 2 - 89  Results-"Congratulations, both grades are passing!"
            */


            //Problem #2 - Logical Operators: Brunch Bunch

            Console.WriteLine("\r\nWelcome to Brunch Bunch!\r\n");

            //Variable used to store users input
            string ageString;

            //Age as an int is stored
            int ageValue;
            decimal specialPrice = 10.00m;
            decimal regularPrice = 15.00m;

            //Prompt the user for their age
            Console.WriteLine("Please enter your age to find your cost of brunch.");
            ageString = Console.ReadLine();

            //Conditional validates no spaces/blank or verifies it's a number
            while ((string.IsNullOrWhiteSpace(ageString)) ||
                (!(int.TryParse(ageString, out ageValue))))
            {
                Console.WriteLine("Please enter a valid number.");
                ageString = Console.ReadLine();
            }

            //String variable to reduce redundancy
            string costMessage = "Your cost for brunch today is $";

            //Conditional to check if age is under 10 OR 55 and older
            if ((ageValue < 10) || (ageValue >= 55))
            {
                Console.WriteLine(costMessage + specialPrice);
            }
            else
            {
                Console.WriteLine(costMessage + regularPrice);
            }

            /*
            Data Sets To Test:
            Age – 57 Results – “Your cost for brunch today is $10.00"
            Age – 39 Results – “Your cost for brunch today is $15.00"
            Age – 8  Results – “Your cost for brunch today is $10.00"
            Age – 10 Results – “Your cost for brunch today is $15.00"

            Self Test:
            Age - 55 Results - "Your cost for brunch today is $10.00"
            */


            //Problem #3 - For Loop: Add Them Up

            Console.WriteLine("\r\nWelcome to Add Them Up!\r\n");

            //Variables to store the string value of the user's input
            string amountOfDvdsString;
            string amountOfBluRayString;
            string amountOfDigUVString;

            //Create a number array to store converted values
            int[] numOfMoviesArray = new int[3];

            int totalMovies = 0;

            //String to reduce code
            string enterValidNum = "Please enter a valid number.";

            //Prompt the user
            Console.WriteLine("Please enter the number of DVDs you have.");
            amountOfDvdsString = Console.ReadLine();

            //Conditional checks for validity
            while ((string.IsNullOrWhiteSpace(amountOfDvdsString)) || 
                (!(int.TryParse(amountOfDvdsString, out numOfMoviesArray[0]))))
            {
                //Interact with the user
                Console.WriteLine(enterValidNum);
                amountOfDvdsString = Console.ReadLine();
            }

            Console.WriteLine("Please enter the number of Blu-Rays you have.");
            amountOfBluRayString = Console.ReadLine();

            //Conditional checks for validity
            while ((string.IsNullOrWhiteSpace(amountOfBluRayString)) ||
                (!(int.TryParse(amountOfBluRayString, out numOfMoviesArray[1]))))
            {
                Console.WriteLine(enterValidNum);
                amountOfBluRayString = Console.ReadLine();
            }

            Console.WriteLine("Please enter the number of Digital UltraViolet copies you have.");
            amountOfDigUVString = Console.ReadLine();

            //Conditional checks for validity
            while ((string.IsNullOrWhiteSpace(amountOfDigUVString)) ||
                (!(int.TryParse(amountOfDigUVString, out numOfMoviesArray[2]))))
            {
                Console.WriteLine(enterValidNum);
                amountOfDigUVString = Console.ReadLine();
            }

            //For loop iterates through array and adds up movie values
            for (int i = 0; i < numOfMoviesArray.Length; i++)
            {
                //adds each element value to the total
                totalMovies += numOfMoviesArray[i];
            }

            //Conditional tests the amount of movies and bases a response
            if (totalMovies >= 100)
            {
                Console.WriteLine($"Wow, I am impressed with your " +
                    $"collection of {totalMovies} movies!");
            }
            else
            {
                Console.WriteLine($"You have a total of {totalMovies} " +
                    $"movies in your collection.");
            }

            /*
            Data Sets To Test:
            DVDs – 45 Blu-Rays – 15 UltraViolets – 2
            Results - "You have a total of 62 movies in your collection."
            DVDs – 60 Blu-Rays – 75 UltraViolets – 45
            Results - "Wow, I am impressed with your collection of 180 movies!"

            Self Test:
            DVDs – 40 Blu-Rays – 40 UltraViolets – 20
            Results - "Wow, I am impressed with your collection of 100 movies!"
            */


            //Problem $4 - While Loop: Cha-Ching!

            Console.WriteLine("\r\nWelcome to Cha-Ching!\r\n");

            //Variables used to store user prompts
            string cardAmountString;
            string currentPurchaseString;

            //Variable will store the purchase amounts
            decimal purchaseTally = 0;
            decimal purchaseCost = 0;

            //Prompt the user
            Console.WriteLine("Please enter your initial Gift Card amount.");
            cardAmountString = Console.ReadLine();

            //Conditional checks for validity
            while ((string.IsNullOrWhiteSpace(cardAmountString)) ||
                (!(decimal.TryParse(cardAmountString, out purchaseTally))))
            {
                Console.WriteLine("Enter a valid cost.");
                cardAmountString = Console.ReadLine();
            }

            //While loop deducts the entries from the card balance and continues until 0
            while (purchaseTally > 0)
            {
                //Ask user how much each purchase is and store it
                Console.WriteLine("How much is your current purchase?");
                currentPurchaseString = Console.ReadLine();

                //Checks validity of user input
                while ((string.IsNullOrWhiteSpace(currentPurchaseString)) ||
                    (!(decimal.TryParse(currentPurchaseString, out purchaseCost))))
                {
                    Console.WriteLine("Enter a valid cost.");
                    currentPurchaseString = Console.ReadLine();
                }

                //Calculates the dropping card balance
                purchaseTally -= purchaseCost;

                //Break - Condition if balance falls to 0 or below
                //if we get to 0 then break it
                if (purchaseTally <= 0)
                {
                    //Calculation figures the amount owed after card is depleted
                    decimal moneyOwed = 0 - purchaseTally;

                    //Informs user of the last purchase and how much is owed
                    Console.WriteLine($"With your last purchase of ${purchaseCost}, " +
                        $"you have used your gift card up and still owe ${moneyOwed}.\r\n");
                    break;
                }

                //Running tally info
                Console.WriteLine($"With your current purchase of ${purchaseCost}, " +
                    $"you can still spend ${purchaseTally}.");
            }

            /*
            Data Sets To Test:
            Gift Card Amount – 30.00
                Purchase1- 5.00
                    Results - "With your current purchase of $5.00, you can still spend $25.00."
                Purchase2- 10.50
                    Results - "With your current purchase of $10.50, you can still spend $14.50."
                Purchase3- 16.00
                    Results - "With your last purchase of $16.00, 
                    you have used your gift card up and still owe $1.50."

            Test Set:
            Gift Card Amount - 50.00
                Purchase1- 28.00
                    Results - "With your current purchase of $28.00, you can still spend $22.00."
                Purchase2- 7.06
                    Results - "With your current purchase of $7.06, you can still spend $14.94."
                Purchase2- 18.45
                    Results - "With your last purchase of $18.45, 
                    you have used your gift card up and still owe $3.51."
            */
        }
    }
}
