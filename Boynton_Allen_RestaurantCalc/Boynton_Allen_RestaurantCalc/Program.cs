﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI Section #03
             11/30/2017
             Restaurant Calculator Assignment
            */

            //Code is refactored for additions or subtractions of number of checks

            //Arrays to iterate through
            string[] checkPositions = new string[] {"first check","second check","third check" };

            //Declared arrays
            //Decimals are recommended when using money values
            string[] checkAmounts = new string[3];
            string[] tipPercentages = new string[3];
            decimal[] amountOfTips = new decimal[3];
            decimal[] checkTotals = new decimal[3];

            decimal totalAmountOfTips;
            decimal allCheckTotals;
            decimal amountOfSplitCheck;

            //Simple welcome note
            Console.WriteLine("Welcome to Restaurant Calculator!");

            //For loop will iterate through each of the arrays to collect or display data
            //Loop eliminates redundant repeated code
            for (int i = 0; i < checkPositions.Length; i++)
            {
                //Prompts the user for the amount of the check
                Console.WriteLine("\nPlease enter the amount of your " + checkPositions[i] + ":");
                checkAmounts[i] = Console.ReadLine();

                //Prompts the user for the tip percentage
                Console.WriteLine("\nPlease enter the percentage you would like to tip:");
                tipPercentages[i] = Console.ReadLine();

                //Amount of the tip is calculated from the amount of the check multiplied by the percentage. Appending array for total tip amount
                amountOfTips[i] = decimal.Parse(checkAmounts[i]) * decimal.Parse(tipPercentages[i]) / 100;
               
                //Amount of the total check is calculated by adding the amount of the tip to the check amount and displayed
                checkTotals[i] = Math.Round(decimal.Parse(checkAmounts[i]) + amountOfTips[i], 2);
                Console.WriteLine("\nThe total for the " + checkPositions[i] + " is $" + checkTotals[i]);
            }

            //Amount of all tips are added together and displayed
            totalAmountOfTips = Math.Round(amountOfTips[0] + amountOfTips[1] + amountOfTips[2], 2);
            Console.WriteLine("\nThe total tip for the waiter is $" + totalAmountOfTips);

            //Totals of all checks are added together and amount is displayed in the console
            allCheckTotals = checkTotals[0] + checkTotals[1] + checkTotals[2];
            Console.WriteLine("\nIf one person paid for all " + checkAmounts.Length + " checks, the total would be $" + allCheckTotals);

            //Total amount of all checks is divided by the length of the checksAmounts array to determine number of ways split. Amount displayed
            amountOfSplitCheck = Math.Round(allCheckTotals / checkAmounts.Length, 2);
            Console.WriteLine("\nIf the check were to be split up " + checkAmounts.Length + " ways, the total for each would be: $" + amountOfSplitCheck + "\n");
            
            /*
             Test #1
             Inputs
             • Check #1 – 10.00 Tip % – 15
             • Check#2 – 15.00 Tip % – 15
             • Check#3 – 25.00 Tip % – 20
             Results
             • Total For Check #1 – $11.50
             • Total For Check #2 – $17.25
             • Total For Check #3 – $30.00
             • Total Tip For The Waiter – $8.75
             • Grand total With Tips – $58.75
             • Cost per person if split evenly 3 ways – $19.58333 or $19.58 Rounded

             Test #2
             Inputs
             • Check #1 – 24.50 Tip % – 15
             • Check#2 – 17.25 Tip % – 25
             • Check#3 – 31.00 Tip % – 20
             Results
             • Total For Check #1 – $28.175 or $28.18 Rounded
             • Total For Check #2 – $21.5625 or $21.56 Rouded
             • Total For Check #3 – $37.20
             • Total Tip For The Waiter – $14.1875 or $14.19 Rounded
             • Grand total With Tips – $86.9375 or $86.94 Rounded
             • Cost per person if split evenly 3 ways – $28.9791666 or $28.98 Rounded
            */
        }
    }
}
