﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI1712 Section #03
             12/10/2017
             ArrayLists Assignment
             */

            //Call Custom method
            DogBreedList();
        }

        //Create a custom method to contain arrayLists
        //Is called from the Main method
        static void DogBreedList()
        {
            //Create arrayList of strings
            ArrayList breedList = new ArrayList() { "PitBull", "Boxer", "Miniature-Pinscher", "Irish Setter", "Schipperke" };

            //Create second arrayList of strings describing elements in first arrayList
            ArrayList namesList = new ArrayList() { "Kennah", "Duke", "Diego", "Champ", "Marley" };

            //Foreach loop iterates through loop while second list is incremented
            int i = 0;
            foreach (Object breed in breedList)
            {
                //Output of each corresponding element in a sentence
                Console.WriteLine($"{namesList[i++]} was the name of my {breed}.");
            }

            //Create a line space to separate groups of loop outputs
            Console.WriteLine("\r\n");

            //Decided to remove the last 2 of the list
            //Used conditional so it would not be hard coded and crash if not enough elements
            if (breedList.Count > 2 && namesList.Count > 2)
            {
                breedList.RemoveRange(breedList.Count - 2, 2);
                namesList.RemoveRange(namesList.Count - 2, 2);
            }

            //Add new elements to both lists at the beginning
            breedList.Insert(0, "Dachshund");
            namesList.Insert(0, "Mylie");

            //Foreach loop iterates through loop while second list is incremented
            int index = 0;
            foreach (Object breed in breedList)
            {
                //Output of each corresponding element in a sentence
                Console.WriteLine($"{namesList[index++]} was the name of my {breed}.");
            }
        }
    }
}
