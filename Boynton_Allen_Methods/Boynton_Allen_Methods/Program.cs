﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI1712 Section #03
             12/08/2017
             Methods Assignment
             */

            //Problem #1: Space Cadet

            Console.WriteLine("\r\nWelcome to Space Cadet!\r\n");

            //Prompt user for their weight
            Console.WriteLine("Please enter your weight in pounds.");
            //Store string value in variable
            string userWeightString = Console.ReadLine();
            //Declare weight as a double - using double because a conversion may lead to decimal places
            double userWeight;

            //Conditional to check that user input is a number
            while (!double.TryParse(userWeightString, out userWeight))
            {
                //Prompt user again for a valid number
                Console.WriteLine("Please only type in numbers!\r\nWhat is your weight in pounds?");
                userWeightString = Console.ReadLine();
            }

            //Call method, introduce argument as userWeight and store in a variable
            double userMoonWeight = ConvertWeightToMoonWeight(userWeight);

            //Display result to the user
            Console.WriteLine($"On Earth you weigh {userWeightString} lbs, but on the moon you would only weigh {userMoonWeight} lbs.\r\n");

            /*
             Data Sets To Test
                Astronaut’s Weight – 160
                    Results - “On Earth you weigh 160 lbs, but on the moon you would only
                    weigh 26.67 lbs.”
                Astronaut’s Weight – 210
                    Results - “On Earth you weigh 210 lbs, but on the moon you would only
                    weigh 35 lbs.”
                Astronaut’s Weight – One Hundred Pounds
                    Results – Re-prompt for number value.
                Astronaut’s Weight – 224.5
                    Results - “On Earth you weigh 224.5 lbs, but on the moon you would only
                    weigh 37.42 lbs.
             */


            //Problem #2: Discount Calculator

            Console.WriteLine("Welcome to Discount Calculator\r\n");

            //Prompt user for the first item cost. Store in a variable
            Console.WriteLine("Enter the cost of your first item: ");
            string itemCost1String = Console.ReadLine();

            //Declare a decimal - Best for money
            decimal itemCost1;

            //Conditional for try parsing string conversion
            while (!decimal.TryParse(itemCost1String, out itemCost1))
            {
                //Prompt the user if no or number entered was not numbers
                Console.WriteLine("Please only type in numbers!\r\nWhat is the cost of your first item?");
                itemCost1String = Console.ReadLine();
            }

            //Prompt for second cost. Store in variable
            Console.WriteLine("Enter the cost of your second item: ");
            string itemCost2String = Console.ReadLine();

            //Declare second cost decimal
            decimal itemCost2;

            //Conditional for converting string to number - verify a number is entered
            while (!decimal.TryParse(itemCost2String, out itemCost2))
            {
                //Re-prompt user if number is invalid
                Console.WriteLine("Please only type in numbers!\r\nWhat is the cost of your second item?");
                itemCost2String = Console.ReadLine();
            }

            //Prompt user for discount in whole number
            Console.WriteLine("What is the discount %?");
            string discountAmountString = Console.ReadLine();

            //Declare percentage value
            decimal discountAmount;

            //Conditional to verify user entry is valid
            while (!decimal.TryParse(discountAmountString, out discountAmount))
            {
                //Displays to the user if they do not enter a valid number
                Console.WriteLine("Please only type in numbers!\r\nWhat is the discount in %?");
                //Stores the users entry 
                discountAmountString = Console.ReadLine();
            }

            //Create variable to store result of given method to calculate total
            decimal discountedTotal = CalculateCostWithDiscount(itemCost1, itemCost2, discountAmount);

            //Create output to the user
            Console.WriteLine($"With a {discountAmount}% discount your total is ${discountedTotal}\r\n");

            /*
             Data Sets To Test
                Cost 1 – 10.00 Cost 2- 15.50 Discount % - 20
                    Results - “With a 20% discount your total is $20.40”
                Cost 1 – 20.25 Cost 2- 37.75 Discount % - 10
                    Results - “With a 10% discount your total is $52.20”
                Cost 1 – 579.99 Cost 2- 348.33 Discount % - 25
                    Results - “With a 25% discount your total is $696.24”
             */

            //Project #3: Double The Fun

            Console.WriteLine("Welcome to Double The Fun!\r\n");

            //Declare and define an array of 5 integers
            int[] integerArray = new int[] { 4, 13, 42, 75, 144 };
            
            //Output results to the console
            //Used String.Join to print the completed, converted new array in its entirety
            Console.WriteLine("The original array was [" + String.Join(", ", integerArray) +
                "] and now it is [" + String.Join(", ", DoubleIntegers(integerArray, 2)) + "]\r\n");

            /*
             Data Sets To Test
                Initial array – [1, 2, 5, 6, 9]
                    Results - Your original array was [1, 2, 5, 6, 9] and now it is doubled it is
                    [2, 4, 10, 12, 18]
                Initial array – [13, 3, 8, 20, 7]
                    Results - Your original array was [12, 3, 8, 20, 7] and now it is doubled it is
                    [24, 6, 16, 40, 14 ]
                Initial array – [4, 13, 42, 75, 144]
                    Results - Your original array was [4, 13, 42, 75, 144] and now it is doubled it is
                    [8, 26, 42, 75, 144]
             */
        }

        //Problem #1 method
        //Create method to calculate weight conversion on the moon
        //Create parameters and create a return as a double
        public static double ConvertWeightToMoonWeight(double weight)
        {
            //Store the result of the calculation in a double
            double moonWeight = Math.Round(weight * 1 / 6, 2);
            //Return as a double
            return moonWeight;
        }

        //Problem #2 method
        //Create method to calculate total cost minus discounted total
        //Create parameters and create a return as a decimal
        public static decimal CalculateCostWithDiscount(decimal cost1,decimal cost2,decimal discount)
        {
            //Calculate cost X 2, then remove the discount from that total
            decimal totalCost = cost1 + cost2;
            decimal discountTotal = Math.Round(totalCost - (totalCost * (discount / 100)), 2);
            //Return as a decimal
            return discountTotal;
        }

        //Problem #3 method
        //Created to handle any size array
        //Create method that recieves an array of ints
        //Cycle through the array and multiply each element by 2
        static int[] DoubleIntegers(int[] numArray, int multiplier)
        {
            //Store the results in a local variable[]
            int[] result = new int[numArray.Length];

            //If the multiplier is not 0 then iterate and assign each element of numArray to stored[]
            if (multiplier != 0)
            {
                //Use a for loop to iterate and calculate one element at a time. Store it in an int[]
                for (int i = 0; i < numArray.Length; i++)
                    //Stores the calculation in an element of the result array
                    result[i] = multiplier * numArray[i];
            }
            //Return as an int array - to store all the entries of the calculated array
            return result;
        }
    }
}
