﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI Section #03
             11/26/2017
             Arrays Assignment
             */

            //Create your own project and call it Lastname_Firstname_Arrays
            //Copy this code inside of this Main section into your Main Section
            //Work through each of the sections

            /* 
              -Use the 2 arrays below
              -Do Not Re-Declare or Re-Define the elements inside of it.
              -Find the total sum of each individual array  
              -Store each sum in a variable and output that variable to console
              
              -Find the average value of each individual array
              -Store each average in a variable and output that variable to console
            */


            //This is the Declare and Definition of the 2 Starting Number Arrays
            int[] firstArray = new int[4] { 7, 24, 30, 12 };
            double[] secondArray = new double[4] { 9, 20.5, 35.9, 237.24 };

            //First part: Find the sum of each array and output to the console ***********************

            //Create a variable for the sum of firstArray and calculate the sum
            int firstArraySum = firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3];

            //Output the sum of the firstArray to the console
            Console.WriteLine("The sum of the elements of firstArray is: " + firstArraySum + "\r\n");

            //Create a variable for the sum of doubles of the secondArray
            double secondArraySum = secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3];

            //Output the sum of the secondArray to the console
            Console.WriteLine("The sum of the elements of secondArray is: " + secondArraySum + "\r\n");

            //Second part: Find the average of each array and output to the console ******************

            //Create a variable for the average of array values of the firstArray
            //Using a double will give an accurate answer if the average is not a whole number
            double firstArrayAverage = (double)firstArraySum / firstArray.Length;

            //Output average of the firstArray to the console
            Console.WriteLine("The average of the elements of firstArray is: " + firstArrayAverage + "\r\n");

            //Create a variable for the average of array values of the secondArray
            double secondArrayAverage = secondArraySum / secondArray.Length;

            //Output average of the secondArray to the console
            Console.WriteLine("The average of the elements of secondArray is: " + secondArrayAverage + "\r\n");


            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                    -This would make the first element of this array = 16
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */

            //Your code for the 3rd Array goes here

            //Declare and define an array of appropriate numbers
            //Use double[] if any added numbers are a double
            double[] thirdArray = new double[4] {firstArray[0] + secondArray[0], firstArray[1] + secondArray[1], firstArray[2] + secondArray[2], firstArray[3] + secondArray[3] };

            //Output thirdArray to the console
            //Used String.Join to print the completed new array in its entirety
            Console.WriteLine("thirdArray = ["+String.Join(",",thirdArray)+"]\r\n");

            /*
               Given the array of strings below named mixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence 
               that is a famous phrase by Bill Gates.
                -Use each element in the array, do not re-write the strings themselves.
                - Do Not Re-Declare or Re-Define the elements inside of it.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            string[] mixedUp = new string[] { " is like measuring ", "Measuring programming progress", "aircraft building progress ", " by lines of code", "by weight." };

            //Is it acceptable to write the datatype name with a capital letter in C#? Should be lowercase M
            //Create a variable to store the concatenated array to read correctly
            string concatString = mixedUp[1] + mixedUp[3] + mixedUp[0] + mixedUp[2] + mixedUp[4];

            //Print concatenated string to the console
            Console.WriteLine("The correctly ordered string reads: "+concatString);
        }
    }
}
