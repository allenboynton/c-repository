﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Allen Boynton
             SDI1712 Section #03
             12/9/2017
             String Objects Assignment
             */

            //Problem #1: Phone Number Checker

            //Prompt the user for a phone number with the example format
            Console.WriteLine("\r\nPlease enter a valid phone number: ex. (###) ###-####");
            string phoneString = Console.ReadLine();

            //Conditional to check for not being blank
            while (string.IsNullOrWhiteSpace(phoneString))
            {
                //Re-prompt user if entry is empty
                Console.WriteLine("Entry was left blank.\r\nPlease enter a phone number to check.");
                phoneString = Console.ReadLine();
            }

            //Call method to verify format of phone number using argument of user input
            string validationMessage = VerifyValidPhoneNumber(phoneString);

            //Display if valid or invalid entry
            Console.WriteLine($"The phone number you entered of {phoneString} is {validationMessage} " +
                $"phone number.\r\n");

            /*
             Data Sets To Test
                User Inputted Phone Number – (123) 456-7890
                    Results - “The phone number you entered of X is a valid phone number.”
                User Inputted Phone Number – (123 456-7890
                    Results - “The phone number you entered of X is an invalid phone
                    number.”
                User Inputted Phone Number – 123-456-7890
                    Results - “The phone number you entered of X is an invalid phone
                    number.”
                User Inputted Phone Number – 1234567890
                    Results - “The phone number you entered of X is an invalid phone
                    number.”
                User Inputted Phone Number – (p23) 456-7890
                    Results - “The phone number you entered of X is an invalid phone
                    number.”
             */


            //Problem #2: Decipher

            //Prompt the user for an encrypted string
            Console.WriteLine("Please enter an encrypted sentence.");
            string encryptString = Console.ReadLine();

            //Conditional verifies that input is not blank
            while (string.IsNullOrWhiteSpace(encryptString))
            {
                //Re-prompt user 
                Console.WriteLine("You did not enter anything.\r\nPlease enter an encrypted sentence.");
                //Store input to string variable
                encryptString = Console.ReadLine();
            }
            //Display result to the user using a method call within the print statement
            Console.WriteLine($"The encrypted text is {encryptString}. Deciphered the text is " +
                $"{DecipherEncryptedString(encryptString)}.");

            /*
             Data Sets To Test
                Encrypted String 1 – “Str^ng *bj#cts c@n b# f+n t* w*rk w^th!”
                    Results – It will be a sentence that makes sense. Try to decrypt them
                    yourself.
                Encrypted String 2 – “D#b+gg^ng ^s th# pr*c#ss *f r#m*v^ng s*ftw@r# b+gs,
                th#n pr*gr@mm^ng m+st b# th# pr*c#ss *f p+tt^ng th#m ^n. “
                    Results – It will be a sentence that makes sense. Try to decrypt them
                    yourself.
                Encrypted String 1 – “Th# ch@ll#ng# I h@d w@s t* cr#@t# @ s#c*nd f*r l**p
                f*r th# tw* st*r#d @rr@ys.”
                    Results – It will be a sentence that makes sense. Try to decrypt them
                    yourself.
             */
        }

        //Problem #1: Phone Number Checker Method
        //Uses user input as parameter
        //Returns a string of valid or invalid
        public static string VerifyValidPhoneNumber(string phoneNumber)
        {
            //Declared and defined variables used as return
            string valid = "a valid";
            string invalid = "an invalid";

            //Check if string is a length of 14. If not input is invalid
            if (phoneNumber.Length == 14)
            {
                //Substrings verify locations of the given characters
                string firstParenthesis = phoneNumber.Substring(0, 1);
                string area = phoneNumber.Substring(1, 3);
                string secondParenthesis = phoneNumber.Substring(4, 1);
                string space = phoneNumber.Substring(5, 1);
                string prefix = phoneNumber.Substring(6, 3);
                string dash = phoneNumber.Substring(9, 1);
                string line = phoneNumber.Substring(10, 4);

                //Conditional checks that number sections are numbers
                //Checks if other characters are the ones they should be - if they are equal
                //Compares the input to the location they should be in
                if (int.TryParse(area, out int areaCode) && int.TryParse(prefix, out int prefixCode) &&
                    int.TryParse(line, out int lineCode) && firstParenthesis.Equals("(") && 
                    secondParenthesis.Equals(")") && space.Equals(" ") && dash.Equals("-"))
                {
                    //All checks are good
                    return valid;
                }
                else
                {
                    //1 or more of the checks are invalid
                    return invalid;
                }
            }
            else
            {
                //Verifies input is no shorter or longer than 14
                return invalid;
            }
        }

        //Problem #2: Decipher
        //Takes user input as a string
        //Returns a decrypted string
        static string DecipherEncryptedString(string code)
        {
            //Define and declare decipher character arrays to loop through
            char[] vowelArray = new char[] { 'a', 'e', 'i', 'o', 'u' };
            char[] decipherChars = new char[] { '@', '#', '^', '*', '+' };

            //Use ToCharArray to convert string to array.
            char[] charArray = code.ToCharArray();
            
            //Loop through array to find encrypted chars and replace with letters
            for (int i = 0; i < charArray.Length; i++)
            {
                //Get character from array.
                char letter = charArray[i];

                //Second for loop iterates through the 2 stored arrays to eliminate index out of range
                for (int x = 0; x < decipherChars.Length; x++)
                {
                    //Conditional to find encrypted chars 
                    if (letter == decipherChars[x])
                    {
                        //Reassign with letter
                        letter = vowelArray[x];

                        //Change charArray back to new string
                        charArray[i] = letter;
                    }
                }
            }
            //Return code as a new string with altered charArray
            return code = new string(charArray);
        }
    }
}
