﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoyntonAllen_FindErrorsCond
{
    class Program
    {
        static void Main(string[] args)
        {
            /*  
             Allen Boynton
             11/30/2017
             SDI1712 Sec # 03
             Find The Errors Conditionals
            */

            //Declared and defined variables
            string myName = "John Doe";
            string myJob = "\"Cat Wrangler\"";

            //Decimals are best used for cost
            decimal myRatePerCat = 7.50m;
            decimal totalPay = 0;

            //Defined value to start amount of cats
            int numberOfCats = 40;
            bool employed = true; //Make sure you use this variable

            //Text to display to the user on the console - using concatenation
            Console.WriteLine($"Hello!  My name is {myName}.");
            Console.WriteLine($"I'm a {myJob}.");
            Console.WriteLine($"My current assignment has me wrangling {numberOfCats} cats.");
            Console.WriteLine("So, let's get to work!");

            //While Loop will decrement down from the value of 40 cats
            //Loop continues if wrangler is still employed or until cats is > 0
            while (numberOfCats > 0) //Do Not Change This line
            {
                //Need to determine if the wrangler is still working or has been fired
                if (employed)
                {
                    //Pay is incremented by myRatePerCat and added to the totalPay
                    totalPay += myRatePerCat;
                    Console.WriteLine($"I've wrangled another cat and I have made " +
                        $"${totalPay} so far.\r\nOnly {numberOfCats} left!");

                    //Necessary to operate while loop. Decrements numberOfCats by 1
                    numberOfCats--;
                }
                else
                {
                    //Output declares wrangler has reached 5 cats and has been fired
                    Console.WriteLine("I've been fired! Someone else will have to wrangle the rest!");

                    //Once this point is reached, the loop is stopped
                    break;
                }
                
                //Conditional if wrangler gets to 5 cats, he is fired!
                if (numberOfCats == 5)
                {
                    //Wrangler is no longer employed. Will go to the else of the employed if statement
                    employed = false;
                }
            }
        }
    }
}
