﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             Allen Boynton
             SDI Section #03
             11/30/2017
             Madlibs Assignment
            */

            //Variables for user prompted responses
            string occupationString = "";
            string colorString = "";
            string verbString = "";
            string nounString = "";
            string ageString = "";
            string costString = "";
            string randomNumberString = "";

            //Create an array to store the strings presented to the user
            string[] promptArray = new string[] {"Enter an occupation: ","Enter a color: ","Enter a verb: ",
                "Enter a noun: ","Enter an age: ","Enter a cost: ","Enter a random number: " };
            string[] responseArray = new string[] {occupationString,colorString,verbString,
                nounString,ageString,costString,randomNumberString };

            decimal[] numberResponses = new decimal[3];

            //Welcome message/title
            Console.WriteLine("Welcome to Al's Madlibs!");

            //For loop prompts the user for given words or numbers as strings to populate the story
            //by iterrating through the promptArray
            for (int i = 0; i < promptArray.Length; i++)
            {
                //Each of these lines displays to the console looping until the last prompt is reached
                Console.WriteLine(promptArray[i]);
                responseArray[i] = Console.ReadLine();
            }

            //Appends the numberResponse array using the given elements to create number values
            //For loop assigns the elements as it iterates through the array - eliminates repititive code
            for (int i = 0; i < numberResponses.Length; i++)
            {
                numberResponses[i] = decimal.Parse(responseArray[i+4]);
            }

            //Outputs a story to the console
            //Supports narrative text
            //String concatenation is used to populate the variables into the story
            Console.WriteLine("\nEvery morning I took my dog Scooby for a walk around the " +
                "neighborhood. We passed many people along the way including this " + responseArray[0] 
                + " that was working in a garden. I told him I loved the bright " + responseArray[1]
                + " tomatoes. He said he had to " + responseArray[2] + " very, very hard to make them" +
                " grow like that. Scooby and I continued our walk until we saw a great big " 
                + responseArray[3] + " flying overhead. It looked like it had been about " 
                + numberResponses[0] + " years old. It must have cost like $" + numberResponses[1] + 
                " to make. It had been such an interesting morning we decided to walk for another "
                + numberResponses[2] + " minutes...we were so tired when we finally returned home.\r\n");
        }
    }
}
