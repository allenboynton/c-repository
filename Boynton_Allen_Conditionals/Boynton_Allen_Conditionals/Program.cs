﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boynton_Allen_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Allen Boynton
            SDI1712 Section #03
            12/2/2017
            Conditional Problems Assignment
            */

            //Baking Converter

            //1 ounce is equal to 28.34952 grams
            double gramsValue = 28.34952;

            //String value of corresponding weight - used to compare string value entered
            string grams = "g";
            string ounces = "oz";
            
            //Convert weightAmountString to a double
            double numberWeight;

            //Converted number from g to oz or vice versa
            double convertedWeight;

            Console.WriteLine("\r\nWelcome to the Baking Converter!\r\n");

            //Create prompts for the user and store weight in a variable
            Console.WriteLine("Please enter a weight amount that you want converted and press Return.");
            string weightAmountString = Console.ReadLine();

            //While loop checks for empty entry or spaces
            while ((string.IsNullOrWhiteSpace(weightAmountString)) || 
                (!double.TryParse(weightAmountString, out numberWeight)))
            {
                //Inform the user what is happening and give further direction
                Console.WriteLine("Not a valid number.\r\nPlease enter a weight amount.");

                //Get value from user
                weightAmountString = Console.ReadLine();
            }

            //Create prompt for the weight type
            Console.WriteLine("Enter \"g\" for grams or \"oz\" for ounces of your weight and press Return.");
            string weightTypeString = Console.ReadLine();

            //Conditional to verify weight type string is not empty
            while (string.IsNullOrWhiteSpace(weightTypeString))
            {
                //Inform the user what is happening and give further direction
                Console.WriteLine("Please do not leave blank.\r\nPlease enter a weight in \"g\" or \"oz\".");

                //Re-confirm the variable to save response
                weightTypeString = Console.ReadLine();
            }

            //Compare using .Equals
            //Letter case does not matter using .OrdinalIgnoreCase
            while ((!weightTypeString.Equals(grams, StringComparison.OrdinalIgnoreCase)) || 
                (!weightTypeString.Equals(ounces, StringComparison.OrdinalIgnoreCase)))
            {
                //Conditional to check if value is in g or oz
                if (weightTypeString.Equals(grams, StringComparison.OrdinalIgnoreCase))
                {
                    //Calculate converted weight to proper weight type
                    convertedWeight = numberWeight / gramsValue;
                    Console.WriteLine($"The weight entered: {numberWeight}g = {convertedWeight} ounces");
                    break;
                }
                else if (weightTypeString.Equals(ounces, StringComparison.OrdinalIgnoreCase))
                {
                    convertedWeight = numberWeight * gramsValue;
                    Console.WriteLine($"The weight entered: {numberWeight}oz = {convertedWeight} grams");
                    break;
                }

                //Inform the user what is happening and give further direction
                Console.WriteLine("Please enter either \"g\" or \"oz\".");

                //Re-confirm the variable to save response
                weightTypeString = Console.ReadLine();
            }

            /*
             Self Test:
             Inputs - Weight = 99 Units = "oZ"
             Results = "The weight entered: 99oz = 2806.60248 grams"
            */


            //Pizza Per Person

            Console.WriteLine("\r\n\r\nWelcome to Pizza Per Person\r\n");

            //Variables needed for calculation
            string numPizzaOrderedString;
            string numSlicesPerPizzaString;
            string numPartyGuestsString;
            string numSlicesPerGuestString;

            double numPizzaOrdered = 0;
            double numSlicesPerPizza = 0;
            double numPartyGuests = 0;
            double numSlicesPerGuest = 0;

            //Create user prompt variables
            Console.WriteLine("How many pizzas did you order?");
            numPizzaOrderedString = Console.ReadLine();

            while ((string.IsNullOrWhiteSpace(numPizzaOrderedString)) || 
              (!(double.TryParse(numPizzaOrderedString, out numPizzaOrdered))))
            {
                Console.WriteLine("Please enter a valid number.");
                numPizzaOrderedString = Console.ReadLine();
            }

            Console.WriteLine("How many slices per pizza?");
            numSlicesPerPizzaString = Console.ReadLine();

            while ((string.IsNullOrWhiteSpace(numSlicesPerPizzaString)) ||
              (!(double.TryParse(numSlicesPerPizzaString, out numSlicesPerPizza))))
            {
                Console.WriteLine("Please enter a valid number.");
                numSlicesPerPizzaString = Console.ReadLine();
            }

            Console.WriteLine("How many guests will be at the party?");
            numPartyGuestsString = Console.ReadLine();

            while ((string.IsNullOrWhiteSpace(numPartyGuestsString)) ||
              (!(double.TryParse(numPartyGuestsString, out numPartyGuests))))
            {
                Console.WriteLine("Please enter a valid number.");
                numPartyGuestsString = Console.ReadLine();
            }

            Console.WriteLine("How many slices will each guest eat?");
            numSlicesPerGuestString = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(numSlicesPerGuestString) ||
              (!(double.TryParse(numSlicesPerGuestString, out numSlicesPerGuest))))
            {
                Console.WriteLine("Please enter a valid number.");
                numSlicesPerGuestString = Console.ReadLine();
            }

            //Math logic
            double totalSlicesOfPizza = numPizzaOrdered * numSlicesPerPizza;
            double totalSlicesNeeded = numPartyGuests * numSlicesPerGuest;
            double remainderOfSlices = totalSlicesOfPizza - totalSlicesNeeded;

            //Conditional if result is positive (>0) there are excess pieces, else negative (<0) in need of more
            if (remainderOfSlices > 0)
            {
                Console.WriteLine($"Yes, you have enough pizza for your guests with " +
                    $"{remainderOfSlices} slices left over.\r\n\r\n");
            }
            else if (remainderOfSlices < 0)
            {
                Console.WriteLine($"You need at least {remainderOfSlices * -1} " +
                    $"more slices of pizza. You should order more pizza.\r\n\r\n");
            }
            else
            {
                //The results to be given did not figure out for a perfect match
                Console.WriteLine("You have the perfect amount of pieces of pizza for your guests.\r\n\r\n");
            }

            /*
             Self Test:
             Inputs - Pizzas: 8, Slices per Pizza: 12, Guest Count: 31, Slices per Guest: 3
             Results = "Yes, you have enough pizza for your guests with 3 slices left over."
             
            Had to add another deviation:
             Inputs - Pizzas: 10, Slices per Pizza: 5, Guest Count: 10, Slices per Guest: 5
             Results = "You have the perfect amount of pieces of pizza for your guests."
            */


            //Tax Brackets

            Console.WriteLine("Welcome to Tax Brackets\r\n");

            //Prompt user for their income
            Console.WriteLine("Please enter your income in dollars.");
            string userIncomeString = Console.ReadLine();

            //Variable to store the number value of the user income
            uint userIncome;

            //Conditional to verify entry is not empty and a valid number
            while (string.IsNullOrWhiteSpace(userIncomeString) ||
                (!(uint.TryParse(userIncomeString, out userIncome))))
            {
                Console.WriteLine("Please enter a valid income in dollars.");
                userIncomeString = Console.ReadLine();
            }

            //To assign string used within each condition for each tier and tax rate
            string[] tierArray = new string[] { "1", "2", "3", "4", "5", "6", "7" };
            string[] rateArray = new string[] { "10%", "15%", "25%", "28%", "33%", "35%", "39.6%" };

            //Create a conditional with if/else to determine the income bracket
            if (userIncome < 9000)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[0]} and your tax rate is {rateArray[0]}.");
            }
            else if (userIncome < 37950)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[1]} and your tax rate is {rateArray[1]}.");
            }
            else if (userIncome < 91900)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[2]} and your tax rate is {rateArray[2]}.");
            }
            else if (userIncome < 191650)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[3]} and your tax rate is {rateArray[3]}.");
            }
            else if (userIncome < 416700)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[4]} and your tax rate is {rateArray[4]}.");
            }
            else if (userIncome < 418400)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[5]} and your tax rate is {rateArray[5]}.");
            }
            else if (userIncome >= 418400)
            {
                Console.WriteLine($"You have an income of ${userIncomeString} " +
                    $"which puts you in tier {tierArray[6]} and your tax rate is {rateArray[6]}.");
            }

            /*
             Self Test:
             Inputs - 87654
             Results = "You have an income of $87654 which puts you in tier 3 and your
             tax rate is 25%."
            */


            //Discount Double Check

            Console.WriteLine("\r\n\r\nWelcome to Discount Double Check\r\n");

            //Variables needed for user prompt strings and number conversion
            string[] itemCostString = new string[2];
            decimal[] itemCost = new decimal[2];

            //Prompt user for cost of items
            Console.WriteLine("Please enter cost of your first item in dollars.");
            itemCostString[0] = Console.ReadLine();

            //Conditional to verify entry is not empty and a valid number
            while (string.IsNullOrWhiteSpace(itemCostString[0]) ||
                (!(decimal.TryParse(itemCostString[0], out itemCost[0]))))
            {
                Console.WriteLine("Please enter a valid cost in dollars.");
                itemCostString[0] = Console.ReadLine();
            }

            Console.WriteLine("Please enter cost of your second item in dollars.");
            itemCostString[1] = Console.ReadLine();

            //Conditional to verify entry is not empty and a valid number
            while (string.IsNullOrWhiteSpace(itemCostString[1]) ||
                (!(decimal.TryParse(itemCostString[1], out itemCost[1]))))
            {
                Console.WriteLine("Please enter a valid cost in dollars.");
                itemCostString[1] = Console.ReadLine();
            }

            //Calculate total of 2 items 
            decimal totalCost = itemCost[0] + itemCost[1];

            //Represent discount amounts
            decimal[] discountPercent = new decimal[] {5, 10 };

            //Variables to store final result - eliminates literal values
            decimal fivePercentOffTotal = Math.Round(totalCost - (totalCost * (discountPercent[0] / 100)), 2);
            decimal tenPercentOffTotal = Math.Round(totalCost - (totalCost * (discountPercent[1] / 100)), 2);

            //Conditional using if/else to isolate the tiers of cost
            if (totalCost < 50)
            {
                //User gets no discount
                Console.WriteLine($"Your total purchase is ${Math.Round(totalCost, 2)}.");
            }
            else if (totalCost < 100)
            {
                //User gets a 5% discount in total purchase
                Console.WriteLine($"Your total purchase is ${fivePercentOffTotal}, " +
                    $"which includes your {discountPercent[0]}% discount.");
            }
            else if (totalCost >= 100)
            {
                //User gets a 10% discount on total purchase
                Console.WriteLine($"Your total purchase is ${tenPercentOffTotal}, " +
                    $"which includes your {discountPercent[1]}% discount.");
            }

            /*
             Self Test:
             Inputs - First Item Cost - $ 125.33, Second Item Cost - $ 75.28
             Results = "Your total purchase is $180.55, which includes your 10%
             discount."
            */
        }
    }
}
